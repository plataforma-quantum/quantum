<?php

return [

    /**
     * Folders
     *
     */

    'folders' => [
        'themes' => base_path('themes'),
        'stubs'  => __DIR__ . '/../stubs'
    ],

    /**
     * Remotes urls
     *
     */

    'remotes' => [
        'plugins' => 'https://gitlab.com/plataforma-quantum/plugins',
        'themes'  => 'https://gitlab.com/plataforma-quantum/themes'
    ],

    /**
     * Tables to be saved on backup
     *
     */

    'backup_tables' => [
        'admin_users',
        'admin_menu',
        'admin_roles',
        'admin_permissions',
        'admin_role_menu',
        'admin_role_permissions',
        'admin_role_users',
        'admin_user_permissions'
    ],

    /**
     * Quantum core services
     *
     */
    'services' => [
        'app'      => Quantum\Services\AppService::class,
        'themes'   => Quantum\Services\ThemeService::class,
        'plugins'  => Quantum\Services\PluginService::class,
        'composer' => Quantum\Services\ComposerService::class,
    ]
];
