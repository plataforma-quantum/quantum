<?php

return [

    /**
     * App commands
     *
     */

    Quantum\Commands\App\InstallCommand::class,
    Quantum\Commands\App\SaveCommand::class,

    /**
     * Themes commands
     *
     */

    Quantum\Commands\Themes\DisableCommand::class,
    Quantum\Commands\Themes\RemoveCommand::class,
    Quantum\Commands\Themes\EnableCommand::class,
    Quantum\Commands\Themes\CreateCommand::class,
    Quantum\Commands\Themes\ListCommand::class,
    Quantum\Commands\Themes\InstallCommand::class,

    /**
     * Plugins commands
     *
     */

    Quantum\Commands\Plugins\CreateCommand::class,
    Quantum\Commands\Plugins\InstallCommand::class,
    Quantum\Commands\Plugins\RemoveCommand::class,
    Quantum\Commands\Plugins\EnableCommand::class,
    Quantum\Commands\Plugins\DisableCommand::class,
];
