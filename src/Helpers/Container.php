<?php

use Quantum\Facades\Service;

if (!function_exists('_q')) {
    function _q(string $namespace)
    {
        return Service::getNamespace($namespace);
    }
}
