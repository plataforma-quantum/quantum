<?php

namespace Quantum\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminRolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_roles')->delete();
        DB::table('admin_roles')->insert(array(
            0 =>
            array(
                'id' => 1,
                'name' => 'Administrator',
                'slug' => 'administrator',
                'created_at' => '2020-05-31 15:34:08',
                'updated_at' => '2020-05-31 15:34:08',
            ),
        ));
    }
}
