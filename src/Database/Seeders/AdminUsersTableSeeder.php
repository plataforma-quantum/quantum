<?php

namespace Quantum\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminUsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_users')->delete();
        DB::table('admin_users')->insert(array(
            0 =>
            array(
                'id' => 1,
                'username' => 'admin',
                'password' => '$2y$10$CY9OLQxiVKgWOV8Q0rgchunV8rQC9CjuHBErdFBsMVCKZ7Pix11Sy',
                'name' => 'Administrator',
                'avatar' => NULL,
                'remember_token' => 'tVOBNTR61c8V3xbiwvPUYDuuLnz4Q62IMNcdRhrVaOE7LvbQK1aOYaul6Cs8',
                'created_at' => '2020-05-31 15:34:08',
                'updated_at' => '2020-05-31 15:34:08',
            ),
        ));
    }
}
