<?php

namespace Quantum\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminMenuTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_menu')->delete();
        DB::table('admin_menu')->insert(array(
            0 =>
            array(
                'id' => 1,
                'parent_id' => 0,
                'order' => 1,
                'title' => 'Painel',
                'icon' => 'fa-dashboard',
                'uri' => '/',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2020-06-01 17:54:28',
            ),
            1 =>
            array(
                'id' => 2,
                'parent_id' => 0,
                'order' => 99,
                'title' => 'Administração',
                'icon' => 'fa-tasks',
                'uri' => NULL,
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2020-06-01 18:00:05',
            ),
            2 =>
            array(
                'id' => 3,
                'parent_id' => 2,
                'order' => 3,
                'title' => 'Usuários',
                'icon' => 'fa-users',
                'uri' => 'auth/users',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2020-06-01 17:58:41',
            ),
            3 =>
            array(
                'id' => 4,
                'parent_id' => 2,
                'order' => 4,
                'title' => 'Níveis de acesso',
                'icon' => 'fa-shield',
                'uri' => 'auth/roles',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2020-06-01 17:58:32',
            ),
            4 =>
            array(
                'id' => 5,
                'parent_id' => 2,
                'order' => 5,
                'title' => 'Permissões',
                'icon' => 'fa-ban',
                'uri' => 'auth/permissions',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2020-06-01 17:58:50',
            ),
            5 =>
            array(
                'id' => 6,
                'parent_id' => 8,
                'order' => 8,
                'title' => 'Menu',
                'icon' => 'fa-bars',
                'uri' => 'auth/menu',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2020-06-01 17:58:07',
            ),
            6 =>
            array(
                'id' => 7,
                'parent_id' => 2,
                'order' => 6,
                'title' => 'Logs',
                'icon' => 'fa-list',
                'uri' => 'auth/logs',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2020-06-01 17:59:08',
            ),
            7 =>
            array(
                'id' => 8,
                'parent_id' => 0,
                'order' => 99,
                'title' => 'Configurações',
                'icon' => 'fa-cogs',
                'uri' => NULL,
                'permission' => '*',
                'created_at' => '2020-06-01 17:57:35',
                'updated_at' => '2020-06-01 17:58:07',
            ),
        ));
    }
}
