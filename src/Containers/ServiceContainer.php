<?php

namespace Quantum\Containers;

use Quantum\Models\Container;

class ServiceContainer
{
    /**
     * Loaded services
     *
     */
    protected $services;

    /**
     * Load services into memory
     *
     */
    public function boot(string $namespace, array $services)
    {
        $this->services[$namespace] = $services;
        return $this;
    }

    /**
     * Get services on namespace
     *
     */
    public function getNamespace(string $namespace)
    {
        return new Container($this->services[$namespace]);
    }
}
