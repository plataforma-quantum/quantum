<?php

namespace Quantum\Models;


class Service
{

    /**
     * Service Model
     *
     * @var Eloquent
     */
    protected $model;

    /**
     * Gets Model Class
     *
     */
    public function getModel()
    {
        return new $this->model;
    }

    /**
     * Finds a Model by Id
     *
     * @param string $id
     * @return Eloquent
     */
    public function find(string $id)
    {
        return call_user_func_array([$this->model, 'find'], [$id]);
    }

    /**
     * Finds a Model by Id
     *
     * @param string $id
     * @return Eloquent
     */
    public function findOrFail(string $id)
    {
        return call_user_func_array([$this->model, 'findOrFail'], [$id]);
    }

    /**
     * Get by column value
     *
     */
    public function findBy(string $column, string $operator, string $value)
    {
        return $this->getModel()->where($column, $operator, $value)->get();
    }

    /**
     * Get one by column value
     *
     */
    public function findOneBy(string $column, string $operator, string $value)
    {
        return $this->getModel()->where($column, $operator, $value)->firstOrFail();
    }

    /**
     * Get all model records
     *
     * @return array
     */
    public function findAll()
    {
        return call_user_func_array([$this->model, 'get'], []);
    }

    /**
     * Set relationships
     *
     */
    public function with($params)
    {
        return call_user_func_array([$this->model, 'with'], [$params]);
    }

    /**
     * Paginate model results
     *
     * @param string $limit
     * @return array
     */
    public function paginate(string $limit)
    {
        return call_user_func_array([$this->model, 'paginate'], [$limit]);
    }

    /**
     * Create a new register
     *
     * @param array $data
     * @return Eloquent
     */
    public function create(array $data)
    {
        return call_user_func_array([$this->model, 'create'], [$data]);
    }

    /**
     * Updates a register
     *
     * @param string $id
     * @param array $data
     * @return Eloquent
     */
    public function update(string $id, array $data)
    {
        return $this->find($id)->update($data);
    }

    /**
     * Create or update a register
     *
     * @param string $id
     * @param array $data
     * @return Eloquent
     */
    public function updateOrCreate(array $condition, array $data)
    {
        return call_user_func_array([$this->model, 'updateOrCreate'], [$condition, $data]);
    }

    /**
     * Delete a register
     *
     * @param string $id
     * @return Eloquent
     */
    public function delete(string $id)
    {
        return call_user_func_array([$this->model, 'destroy'], [$id]);
    }

    /**
     * Listen to simple calls
     *
     */
    public function __call($name, $arguments)
    {
        return call_user_func_array([$this->getModel(), $name], $arguments);
    }

    /**
     * Listen to static calls
     *
     */
    public static function __callStatic($name, $arguments)
    {
        return call_user_func_array([(new static)->getModel(), $name], $arguments);
    }
}
