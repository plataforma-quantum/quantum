<?php

namespace Quantum\Models;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Quantum\Traits\Models\HasDependencies;
use Illuminate\Support\Str;
use ZipArchive;

class Theme
{
    use HasDependencies;

    /**
     * Theme name to display
     *
     */
    public $displayName;

    /**
     * Theme name
     *
     */
    public $name;

    /**
     * Theme alias
     *
     */
    public $alias;

    /**
     * Path to theme folder
     *
     */
    public $path;

    /**
     * Info file path
     *
     */
    public $infoFile;

    /**
     * Theme remote url
     *
     */
    public $remoteUrl;

    /**
     * Themes folder path
     *
     */
    public $themesFolder;

    /**
     * Constructor method
     *
     */
    public function __construct(string $alias)
    {
        $this->displayName = Str::ucfirst(Str::camel($alias));
        $this->path = config('quantum.folders.themes') . '/' . $this->displayName;
        $this->infoFile = config('quantum.folders.themes') . '/themes.json';
        $this->name = $this->displayName . 'Theme';
        $this->alias = Str::lower(Str::kebab($alias));
        $this->remoteUrl = sprintf('%s/%s-theme/-/archive/master/%s-theme-master.zip', config('quantum.remotes.themes'), $this->alias, $this->alias);
        $this->themesFolder = base_path('themes');
    }

    /**
     * Download zip theme into folder
     *
     */
    function download()
    {
        $zipFilePath = sprintf('%s/%s.zip', $this->themesFolder, $this->displayName);
        file_put_contents($zipFilePath, file_get_contents($this->remoteUrl));
        return $this;
    }

    /**
     * Extract theme
     *
     */
    function extract()
    {
        // Initialize paths
        $folderFromName = sprintf('%s/%s-theme-master', $this->themesFolder, $this->alias);
        $zipFilePath = sprintf('%s/%s.zip', $this->themesFolder, $this->displayName);

        // Extract file
        $zip = new ZipArchive;
        $zip->open($zipFilePath);
        $zip->extractTo($this->themesFolder);
        $zip->close();

        // Rename new folder and remove zip file
        rename($folderFromName, $this->path);
        unlink($zipFilePath);

        // Returns a instance
        return $this;
    }

    /**
     * Generate the theme on themes folder
     *
     */
    public function generate()
    {
        // Load stubs file
        $files = Storage::disk('stubs')->allFiles('theme');

        // Create the template
        $tpl = [
            '[%THEME_NAME%]' => $this->displayName,
            '[%THEME_NAME_LOWERCASE%]' => Str::lower(Str::kebab($this->displayName)),
            '[%THEME_PROVIDER%]' => addslashes($this->getProvider())
        ];

        // Copy each file from stub to themes folder, replacing the variables
        foreach ($files as $file) {

            // Get file path in themes folder
            $filePath = $this->displayName . "/" . strtr(trim($file, 'theme'), $tpl);

            // Replace its contents
            $content = Storage::disk('stubs')->get($file);
            $newContent = strtr($content, $tpl);

            // Store the file
            Storage::disk('themes')->put(Str::replaceLast('.stub', '', $filePath), $newContent);
        }

        // Return the instance
        return $this;
    }

    /**
     * Remove theme dir
     *
     */
    public function remove()
    {
        (new Filesystem)->deleteDirectory($this->path);
        return $this;
    }

    /**
     * Check if a theme is installed
     *
     */
    public function installed()
    {
        if ((new Filesystem)->isDirectory($this->path)) {
            return true;
        }
        return false;
    }

    /**
     * Enable theme
     *
     */
    public function enable()
    {
        $content = json_decode(file_get_contents($this->infoFile), true);
        $content[$this->displayName] = true;
        file_put_contents($this->infoFile, json_encode($content, JSON_PRETTY_PRINT));
        return $this;
    }

    /**
     * Disable theme
     *
     */
    public function disable()
    {
        $content = json_decode(file_get_contents($this->infoFile), true);
        $content[$this->displayName] = false;
        file_put_contents($this->infoFile, json_encode($content, JSON_PRETTY_PRINT));
        return $this;
    }

    /**
     * Check if the theme is enabled
     *
     */
    public function enabled()
    {
        $content = json_decode(file_get_contents($this->infoFile), true);
        return optional($content)[$this->displayName];
    }

    /**
     * Check if the theme is disabled
     *
     */
    public function disabled()
    {
        return !$this->enabled();
    }

    /**
     * Get theme service provider
     *
     */
    public function getProvider()
    {
        return "\\Themes\\{$this->displayName}\\Providers\\{$this->name}ServiceProvider";
    }
}
