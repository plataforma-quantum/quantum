<?php

namespace Quantum\Models;

use Quantum\Traits\Models\HasDependencies;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use ZipArchive;
use Module;

class Plugin
{
    use HasDependencies;

    /**
     * Plugin alias
     *
     */
    public $alias;

    /**
     * Plugin name
     *
     */
    public $name;

    /**
     * Git remote url
     *
     */
    public $remoteUrl;

    /**
     * Plugins folder
     *
     */
    public $pluginsFolder;

    /**
     * Path to plugin folder
     *
     */
    public $path;

    /**
     * Constructor method
     *
     */
    function __construct(string $alias)
    {
        $this->remoteUrl = sprintf('%s/%s-plugin/-/archive/master/%s-plugin-master.zip', config('quantum.remotes.plugins'), $alias, $alias);
        $this->name = Str::ucfirst(Str::camel($alias));
        $this->pluginsFolder = base_path('plugins');
        $this->alias = $alias;
        $this->path = sprintf('%s/%s', $this->pluginsFolder, $this->name);
    }

    /**
     * Download zip plugin into folder
     *
     */
    function download()
    {
        $zipFilePath = sprintf('%s/%s.zip', $this->pluginsFolder, $this->name);
        file_put_contents($zipFilePath, file_get_contents($this->remoteUrl));
        return $this;
    }

    /**
     * Extract plugin
     *
     */
    function extract()
    {
        // Initialize paths
        $folderFromName = sprintf('%s/%s-plugin-master', $this->pluginsFolder, $this->alias);
        $zipFilePath = sprintf('%s/%s.zip', $this->pluginsFolder, $this->name);

        // Extract file
        $zip = new ZipArchive;
        $zip->open($zipFilePath);
        $zip->extractTo($this->pluginsFolder);
        $zip->close();

        // Rename new folder and remove zip file
        rename($folderFromName, $this->path);
        unlink($zipFilePath);

        // Returns a instance
        return $this;
    }

    /**
     * Enable plugin
     *
     */
    function enable()
    {
        $module = call_user_func([Module::class, 'find'], $this->name);
        $module->enable();
        return $this;
    }

    /**
     * Disable plugin
     *
     */
    function disable()
    {
        $module = call_user_func([Module::class, 'find'], $this->name);
        $module->disable();
        return $this;
    }

    /**
     * Remove plugin
     *
     */
    function remove()
    {
        $dirname = sprintf('%s/%s', $this->pluginsFolder, $this->name);
        (new Filesystem)->deleteDirectory($dirname);
        return $this;
    }

    /**
     * Check if plugin installed
     *
     */
    function installed()
    {
        return call_user_func([Module::class, 'has'], $this->name);
    }
}
