<?php

namespace Quantum\Models;

class Container
{
    /**
     * Services instances
     *
     */
    protected $services;

    /**
     * Constructor method
     *
     */
    public function __construct($services)
    {
        $this->services = $services;
    }

    /**
     * Get service instance
     *
     */
    public function service(string $alias)
    {
        return app($this->services[$alias]);
    }
}
