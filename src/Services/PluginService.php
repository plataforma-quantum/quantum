<?php

namespace Quantum\Services;

use Quantum\Models\Plugin;

class PluginService
{
    /**
     * Opened plugin
     *
     */
    public $plugin;

    /**
     * Open a new plugin
     *
     */
    function open(string $alias)
    {
        $this->plugin = new Plugin($alias);
        return $this->plugin;
    }
}
