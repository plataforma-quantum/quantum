<?php

namespace Quantum\Services;

use Error;
use Illuminate\Contracts\Cache\Store;
use Illuminate\Support\Facades\Storage;
use Quantum\Models\Theme;

class ThemeService
{
    /**
     * Opened theme
     *
     */
    public $theme;

    /**
     * Open a theme
     *
     */
    public function open(string $alias)
    {
        $this->theme = new Theme($alias);
        return $this->theme;
    }

    /**
     * Finds a theme by name
     *
     */
    public function find(string $name)
    {
        $filtered = collect($this->findAll())->filter(fn ($theme) => $theme->displayName === $name);
        return $filtered->count() === 1 ? $filtered->first() : null;
    }

    /**
     * Get all themes
     *
     */
    public function findAll()
    {
        return collect(Storage::disk('themes')->directories())->map(function ($name) {
            return $this->open($name);
        });
    }
}
