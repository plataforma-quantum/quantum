<?php

namespace Quantum\Commands\Plugins;

use Illuminate\Console\Command;

class RemoveCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quantum:plugin-remove {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes a plugin';

    /**
     * Plugin Service Instance
     *
     */
    protected $pluginService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->pluginService = _q('core')->service('plugins');
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('Removing plugin...');

        // Load plugin
        $plugin = $this->pluginService->open('cep');

        // Check if it is already downloaded
        if (!$plugin->installed()) {
            $this->error('Plugin is not installed');
            return;
        }

        // If it is not downloaded yet, download it
        $plugin->disable()->remove();
        $this->info('Plugin removed with success!');
        return;
    }
}
