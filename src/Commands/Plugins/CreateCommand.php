<?php

namespace Quantum\Commands\Plugins;

use Illuminate\Console\Command;

class CreateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quantum:plugin-create {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a plugin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('module:make', [
            'name' => [$this->argument('name')]
        ]);
    }
}
