<?php

namespace Quantum\Commands\Plugins;

use Illuminate\Console\Command;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quantum:plugin-install {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installs a plugin';

    /**
     * Plugin Service Instance
     *
     */
    protected $pluginService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->pluginService = _q('core')->service('plugins');
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('Installing plugin...');

        // Load plugin
        $plugin = $this->pluginService->open($this->argument('name'));

        // Check if it is already downloaded
        if ($plugin->installed()) {
            $plugin->enable();
            $this->info('Plugin installed with success!');
            return;
        }

        // If it is not downloaded yet, download it
        $plugin->download()->extract()->disable();
        $this->line('Installing dependencies...');
        $plugin->installDependencies();
        $plugin->enable();
        $this->info('Plugin installed with success!');
        return;
    }
}
