<?php

namespace Quantum\Commands\App;

use Illuminate\Support\Facades\Storage;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quantum:app-install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install application';

    /**
     * Composer service instance
     *
     */
    protected $composerService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->composerService = _q('core')->service('composer');
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // Create plugins folder
        if (!file_exists(base_path('plugins/plugins.json'))) {
            mkdir(base_path('plugins'));
            file_put_contents(base_path('plugins/plugins.json'), '{}');
        }

        if (!is_dir(base_path('themes'))) {
            mkdir('themes');
        }

        // Create themes folder
        if (!file_exists(base_path('themes/themes.json'))) {
            file_put_contents(base_path('themes/themes.json'), '{}');
        }

        // Publish configuration files
        $this->call('vendor:publish', [
            '--provider' => 'Quantum\\QuantumServiceProvider'
        ]);
        $this->call('vendor:publish', [
            '--provider' => 'Encore\Admin\AdminServiceProvider',
            '--tag'      => 'laravel-admin-lang'
        ]);
        $this->call('vendor:publish', [
            '--provider' => 'Encore\Admin\AdminServiceProvider',
            '--tag'      => 'laravel-admin-migrations'
        ]);
        $this->call('vendor:publish', [
            '--provider' => 'Encore\Admin\AdminServiceProvider',
            '--tag'      => 'laravel-admin-assets'
        ]);

        // Add namespaces to composer
        $this->composerService
            ->open()
            ->addNamespace('Plugins\\', 'plugins/')
            ->addNamespace('Themes\\', 'themes/')
            ->close()
            ->dumpAutoload();

        // Running migrations files
        $this->call('migrate', [
            '--path' => 'database/migrations/2016_01_04_173148_create_admin_tables.php'
        ]);

        // Install admin backend
        $this->call('admin:install', []);

        // Seed database
        $this->call('db:seed', [
            '--class' => 'Quantum\\Database\\Seeders\\DatabaseSeeder'
        ]);
    }
}
