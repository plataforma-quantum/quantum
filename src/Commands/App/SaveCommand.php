<?php

namespace Quantum\Commands\App;

use Illuminate\Console\Command;

class SaveCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quantum:app-save';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Save application state';

    /**
     * Tables to be saved
     *
     */
    protected $tables = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // Load tables to be saved on backups
        $this->tables = config('quantum.backup_tables');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('iseed', ['tables' => join(',', $this->tables)]);
        $this->info('Application state was saved with success');
    }
}
