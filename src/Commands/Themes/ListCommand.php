<?php

namespace Quantum\Commands\Themes;

use Illuminate\Console\Command;

class ListCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quantum:theme-list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List all installed themes';

    /**
     * Theme Service Instance
     *
     */
    protected $themeService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->themeService = _q('core')->service('themes');
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $themes = $this->themeService->findAll();
        $this->table(['Name', 'Enabled'], collect($themes)->map(function ($theme) {
            return [
                'name'    => $theme->displayName,
                'enabled' => $theme->enabled() ? "On" : "Off"
            ];
        })->toArray());
    }
}
