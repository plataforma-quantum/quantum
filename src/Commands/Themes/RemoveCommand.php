<?php

namespace Quantum\Commands\Themes;

use Illuminate\Console\Command;

class RemoveCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quantum:theme-remove {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes a theme';

    /**
     * Theme Service Instance
     *
     */
    protected $themeService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->themeService = _q('core')->service('themes');
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('Removing theme...');
        $theme = $this->themeService->open($this->argument('name'));

        // Check if it is installed
        if (!$theme->installed()) {
            return;
        }

        // Remove theme
        $theme->remove()->disable();
        $this->info('Theme removed with success!');
    }
}
