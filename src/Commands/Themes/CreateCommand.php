<?php

namespace Quantum\Commands\Themes;

use Illuminate\Console\Command;

class CreateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quantum:theme-create {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a theme';

    /**
     * Theme Service Instance
     *
     */
    protected $themeService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->themeService = _q('core')->service('themes');
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('Creating theme...');
        $theme = $this->themeService->open($this->argument('name'));

        // Check if the theme is already installed
        if ($theme->installed()) {
            if (!$this->confirm('Theme is already installed. Do you want to overwrite it?')) {
                return;
            }
            $theme->remove();
        }

        // Creates the theme
        $theme->generate()->enable();
        $this->info('Theme created with success!');
    }
}
