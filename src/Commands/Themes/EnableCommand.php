<?php

namespace Quantum\Commands\Themes;

use Illuminate\Console\Command;
use Throwable;

class EnableCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quantum:theme-enable {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enables a theme';

    /**
     * Theme Service Instance
     *
     */
    protected $themeService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->themeService = _q('core')->service('themes');
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $theme = $this->themeService->open($this->argument('name'));

            if (!$theme->installed()) {
                $this->error('Theme not installed');
                return;
            }

            $this->line('Installing theme dependencies...');
            $theme->installDependencies();

            $theme->enable();
            $this->info(sprintf("Theme %s enabled successfully!", $this->argument('name')));
        } catch (Throwable $e) {
            $this->error($e->getMessage());
        }
    }
}
