<?php

namespace Quantum\Commands\Themes;

use Illuminate\Console\Command;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quantum:theme-install {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installs a theme';

    /**
     * Theme Service Instance
     *
     */
    protected $themeService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->themeService = _q('core')->service('themes');
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('Creating theme...');
        $theme = $this->themeService->open($this->argument('name'));

        // Check if the theme is already installed
        if ($theme->installed()) {
            if (!$this->confirm('Theme is already installed. Do you want to overwrite it?')) {
                return;
            }
            $theme->remove();
        }

        $this->line('Installing theme dependencies...');
        $theme->installDependencies();

        // If it is not downloaded yet, download it
        $theme->download()->extract()->enable();
        $this->info('Theme installe with success!');
    }
}
