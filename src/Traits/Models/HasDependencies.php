<?php

namespace Quantum\Traits\Models;

trait HasDependencies
{

    /**
     * Composer requires
     *
     */
    public $composerRequire = [];

    /**
     * Composer requires dev
     *
     */
    public $composerRequireDev = [];

    /**
     * Plugin require
     *
     */
    public $pluginRequire = [];

    /**
     * File require
     *
     */
    public $fileRequire = [];

    /**
     * Scripts
     *
     */
    public $scripts = [];

    /**
     * Load module composer dependencies
     *
     */
    public function _bootHasDependencies()
    {
        $moduleInfoFile = $this->path . '/module.json';
        if (!file_exists($moduleInfoFile)) {
            return;
        }

        $json = json_decode(file_get_contents($moduleInfoFile), true);

        if (isset($json['require'])) {
            $this->composerRequire = $json['require'];
        }

        if (isset($json['require-dev'])) {
            $this->composerRequireDev = $json['require-dev'];
        }

        if (isset($json['plugins'])) {
            $this->pluginRequire = $json['plugins'];
        }

        if (isset($json['files'])) {
            $this->fileRequire = $json['files'];
        }

        if (isset($json['scripts'])) {
            $this->scripts = $json['scripts'];
        }
    }

    /**
     * Install composer dependencies
     *
     */
    public function composerInstall()
    {
        $this->_bootHasDependencies();

        foreach ($this->composerRequire as $package => $version) {
            if ($version === 'master') {
                $version = "";
            } else {
                $version = "@$version";
            }
            exec("composer require {$package}{$version}");
        }

        foreach ($this->composerRequireDev as $package => $version) {
            if ($version === 'master') {
                $version = "";
            } else {
                $version = "@$version";
            }
            exec("composer require {$package}{$version} --dev");
        }
    }

    /**
     * Install plugin dependencies
     *
     */
    public function pluginInstall()
    {
        $this->_bootHasDependencies();

        foreach ($this->pluginRequire as $plugin => $version) {
            exec("php artisan quantum:plugin-install $plugin");
        }
    }

    /**
     * Install files
     *
     */
    public function fileInstall()
    {
        $this->_bootHasDependencies();

        foreach ($this->fileRequire as $file) {
            _q('core')->service('composer')->open();
            _q('core')->service('composer')->addFile($file);
            _q('core')->service('composer')->close()->dumpAutoload();
        }
    }

    /**
     * Run scripts
     *
     */
    public function runScripts()
    {
        $this->_bootHasDependencies();
        foreach ($this->scripts as $script) {
            exec($script);
        }
    }

    /**
     * Intall package dependencies
     *
     */
    public function installDependencies()
    {
        $this->composerInstall();
        $this->pluginInstall();
        $this->fileInstall();
        $this->runScripts();
    }
}
