<?php

namespace Quantum\Facades;

use Illuminate\Support\Facades\Facade;

class Service extends Facade
{

    /**
     * Get facade accessor
     *
     */
    protected static function getFacadeAccessor()
    {
        return 'Quantum\\Containers\\ServiceContainer';
    }
}
