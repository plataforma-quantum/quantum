<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        @if(config('admin.show_environment'))
        <strong>{{__('Ambiente')}}</strong>&nbsp;&nbsp; {!! config('app.env') !!}
        @endif

        &nbsp;&nbsp;&nbsp;&nbsp;

        @if(config('admin.show_version'))
        <strong>{{__('Versão')}}</strong>&nbsp;&nbsp; 1.0.0
        @endif

    </div>
    <!-- Default to the left -->
    <strong>{{__('Desenvolvido por')}} <a href="https://startaideia.com.br" target="_blank">Startaideia</a></strong>
</footer>
