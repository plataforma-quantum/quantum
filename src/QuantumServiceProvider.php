<?php

namespace Quantum;

use Illuminate\Support\ServiceProvider;
use Quantum\Facades\Service;

class QuantumServiceProvider extends ServiceProvider
{

    /**
     * Register configuration files
     *
     */
    public function registerConfig()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/commands.php', 'quantum::commands');
        $this->mergeConfigFrom(__DIR__ . '/../config/modules.php', 'modules');
        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'quantum');
        $this->mergeConfigFrom(__DIR__ . '/../config/admin.php', 'admin');

        config(['filesystems.disks.themes' => [
            'driver' => 'local',
            'root'   => config('quantum.folders.themes')
        ]]);

        config(['filesystems.disks.stubs' => [
            'driver' => 'local',
            'root'   => config('quantum.folders.stubs')
        ]]);
    }

    /**
     * Register themes
     *
     */
    public function registerThemes()
    {
        $themeService = app('Quantum\\Services\\ThemeService');
        foreach ($themeService->findAll() as $theme) {
            if ($theme->enabled()) {
                $this->app->register($theme->getProvider());
            }
        }
    }

    /**
     * Register bindings
     *
     */
    public function registerBindings()
    {
        $this->app->singleton('Quantum\\Containers\\ServiceContainer', function () {
            return new \Quantum\Containers\ServiceContainer;
        });
    }

    /**
     * Boot views
     *
     */
    public function bootViews()
    {
        $this->app['view']->prependNamespace('admin', __DIR__ . '/Views');
    }

    /**
     * Boot package commands
     *
     */
    public function bootCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands(config('quantum::commands'));
        }
    }

    /**
     * Boot publishes files
     *
     */
    public function bootPublishes()
    {
        $this->publishes([
            __DIR__ . '/../config/modules.php' => config_path('modules.php'),
            __DIR__ . '/../config/admin.php' => config_path('admin.php')
        ], 'config');
        $this->publishes([
            __DIR__ . '/../public/' => public_path('vendor/quantum/')
        ], 'public');
    }

    /**
     * Boot migrations files
     *
     */
    public function bootMigrations()
    {
        $this->loadMigrationsFrom(__DIR__ . '/Database/Migrations');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->registerConfig();
        $this->registerThemes();
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootMigrations();
        $this->bootPublishes();
        $this->bootCommands();
        $this->bootViews();

        // Boot quantum core services
        Service::boot('core', config('quantum.services', []));
    }
}
